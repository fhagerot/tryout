<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title></title>
</head>
<body>
<form method="POST" action="{{route('rename-images')}}" enctype="multipart/form-data">
    @csrf
    <input type="file" name="zip"/>
    <input type="submit" value="Обработать"/>
</form>
</body>
</html>