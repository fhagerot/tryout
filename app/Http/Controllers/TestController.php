<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
include_once(base_path('libs/pclzip.lib.php'));

class TestController extends Controller
{
    public function index()
    {
        return view('main');
    }

    /* Возвращает архив с переводом имен графических файлов с кириллицы на латиницу. */

    public function renameImages(request $request)
    {
        $Path = $request->file('zip')->getPathname();
        $folderName = uniqid();
        $archive = new \PclZips($Path);
        $result = $archive->extract(PCLZIP_OPT_PATH, 'extracted/' . $folderName);
        $replaceArray = [];
        foreach (File::allFiles(public_path('extracted/' . $folderName)) as $file) {
            if (self::isRussianImage($file->getFilename())) {
                $newName = self::translit($file->getFilenameWithoutExtension());
                $ext = $file->getExtension();
                $postFix = "";
                $i = 0;
                while (file_exists($file->getPath() . '/' . $newName . $postFix . '.' . $ext)) {
                    $i++;
                    $postFix = ($i < 10) ? ("_0" . $i) : ("_" . $i);
                }
                $newName .= $postFix . '.' . $ext;
                $replaceArray[] = [
                    'oldName' => $file->getFilename(),
                    'newName' => $newName
                ];
                rename($file->getPathname(), $file->getPath() . '/' . $newName);
            }
        }

        foreach (File::allFiles(public_path('/extracted/' . $folderName)) as $file) {
            if ($file->getExtension() == 'html') {
                foreach ($replaceArray as $arr) {
                    $file_contents = file_get_contents($file->getPathname());
                    $fh = fopen($file, "w");
                    $file_contents = str_replace($arr['oldName'],$arr['newName'],$file_contents);
                    fwrite($fh, $file_contents);
                    fclose($fh);
                }
                $dom = new \DOMDocument();
                $dom->loadHTML(file_get_contents($file->getPathname()));
                $images = $dom->getElementsByTagName('img');

                $pathFileArr =  explode("/", str_replace('\\', '/', stristr($file->getPathname(), 'extracted')));
                unset($pathFileArr[0], $pathFileArr[1], $pathFileArr[2]);
                $from = implode('/', $pathFileArr);
                foreach ($images as $image) {
                    $src = $image->getAttribute('src');
                    if (mb_substr($src, 0, 1) == '/') {
                        $src = self::getRelativePath($from,substr($src, 1));
                    }
                    $image->setAttribute('src', $src);
                    $html = $dom->saveHTML();
                    $fh = fopen($file, "w");
                    fwrite($fh, $html);
                    fclose($fh);
                }
            }
        }
        $fileName = $request->file('zip')->getClientOriginalName();
        $files = glob(public_path('extracted/' . $folderName .'/*'));
        \Zipper::make(public_path($fileName))->add($files)->close();
        $file = public_path($fileName);
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Content-Length: ' . filesize($file));
        if (readfile($file))
        {
            unlink($file);
        }
        return;
    }

    /* Получить относительный путь из файла $from до $to */

    public function getRelativePath($from, $to)
    {
        // some compatibility fixes for Windows paths
        $from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
        $to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
        $from = str_replace('\\', '/', $from);
        $to   = str_replace('\\', '/', $to);

        $from     = explode('/', $from);
        $to       = explode('/', $to);
        $relPath  = $to;

        foreach($from as $depth => $dir) {
            // find first non-matching dir
            if($dir === $to[$depth]) {
                // ignore this directory
                array_shift($relPath);
            } else {
                // get number of remaining dirs to $from
                $remaining = count($from) - $depth;
                if($remaining > 1) {
                    // add traversals up to first matching dir
                    $padLength = (count($relPath) + $remaining - 1) * -1;
                    $relPath = array_pad($relPath, $padLength, '..');
                    break;
                } else {
                    $relPath[0] = './' . $relPath[0];
                }
            }
        }
        return implode('/', $relPath);
    }

    /* Проверяет есть ли кирилица в названии графического файла */

    private static function isRussianImage($text) {
        return preg_match('/[а-яё].[bmp|jpeg|gif|png|jpg]/ui', $text);
    }

    /* Транслитерация с кирилицы на латиницу */

    public static function translit($s) {

        $translit = array(

            'а' => 'a',   'б' => 'b',   'в' => 'v',

            'г' => 'g',   'д' => 'd',   'е' => 'e',

            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',

            'и' => 'i',   'й' => 'j',   'к' => 'k',

            'л' => 'l',   'м' => 'm',   'н' => 'n',

            'о' => 'o',   'п' => 'p',   'р' => 'r',

            'с' => 's',   'т' => 't',   'у' => 'u',

            'ф' => 'f',   'х' => 'h',   'ц' => 'c',

            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',

            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',

            'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya',


            'А' => 'A',   'Б' => 'B',   'В' => 'V',

            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',

            'И' => 'I',   'Й' => 'J',   'К' => 'K',

            'Л' => 'L',   'М' => 'M',   'Н' => 'N',

            'О' => 'O',   'П' => 'P',   'Р' => 'R',

            'С' => 'S',   'Т' => 'T',   'У' => 'U',

            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',

            'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',

            'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',

        );
        return strtr($s, $translit);
    }

}
